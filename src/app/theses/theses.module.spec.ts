import { ThesesModule } from './theses.module';
import { TestBed, async } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ThesesModule', () => {
  let thesesModule: ThesesModule;

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        CommonModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ]
    }).compileComponents().then( () => {
      const translateService = TestBed.get(TranslateService);
      thesesModule = new ThesesModule(translateService);
    });
  }));

  it('should create an instance', () => {
    expect(thesesModule).toBeTruthy();
  });
});
